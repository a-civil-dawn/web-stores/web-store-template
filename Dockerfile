# syntax = docker/dockerfile:1

ARG BASE_IMAGE
FROM $BASE_IMAGE as base

ARG APP_USER
ARG PG_VERSION
ARG NODE_VERSION

# Throw-away build stage to reduce size of final image
FROM base as build

# Switch back to root for build
USER root

# Install packages needed to install other packages
RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
    gnupg2 \
    curl \
  && rm -rf /var/cache/apt/lists/*

# Set up postgres repo
RUN curl -sSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && echo 'deb http://apt.postgresql.org/pub/repos/apt/ bookworm-pgdg main' $PG_VERSION > /etc/apt/sources.list.d/pgdg.list

# Set up nodejs repo
RUN curl -sSL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash -

# Install packages needed to build gems
RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
    build-essential \
    git \
    libpq-dev \
    libvips \
    nodejs \
    pkg-config \
  && rm -rf /var/cache/apt/lists/*

# Install application gems
# Uncomment if we have a local gemfile with overrides
# COPY Gemfile Gemfile.lock ./
# RUN bundle install && \
#     rm -rf ~/.bundle/ "${BUNDLE_PATH}"/ruby/*/cache "${BUNDLE_PATH}"/ruby/*/bundler/gems/*/.git && \
#     bundle exec bootsnap precompile --gemfile

# Copy application-specific code
COPY . .

# Destroy pre-generated assets from the upstream container
RUN SECRET_KEY_BASE_DUMMY=1 bundle exec rake assets:clobber

# Precompile bootsnap code for faster boot times
RUN bundle exec bootsnap precompile app/ lib/

# Precompiling assets for production without requiring secret RAILS_MASTER_KEY
RUN SECRET_KEY_BASE_DUMMY=1 ./bin/rails assets:precompile


# Final stage for app image
FROM base

# Switch back to root for build
USER root

# Destroy pre-generated assets from the upstream container
RUN SECRET_KEY_BASE_DUMMY=1 bundle exec rake assets:clobber

# Copy built artifacts: gems, application
COPY --from=build /usr/local/bundle /usr/local/bundle
COPY --from=build /rails /rails

# Run and own only the runtime files as a non-root user for security
RUN chown -R $APP_USER:$APP_USER /rails

# Gitlab applies umask 0000 which results in all files being checked out 777
# Change the permissions back to something sane
RUN find /rails -type d -exec chmod 750 {} \; \
 && find /rails -type f -exec chmod 640 {} \; \
 && find /rails/bin -type f -exec chmod 750 {} \;

USER $APP_USER:$APP_USER

# Entrypoint prepares the database.
ENTRYPOINT ["/rails/bin/docker-entrypoint"]

# Start the server by default, this can be overwritten at runtime
EXPOSE 3000
CMD ["./bin/rails", "server", "-b", "0.0.0.0"]
