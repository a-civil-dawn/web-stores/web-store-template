#!/bin/bash

if [ $UID -ne 0 ]; then
	echo "This script must run as root" >&2
	exit 1
fi

MOUNT_PATH=/mnt/solidus
WORK_PATH=/tmp/solidus-overlay
BASE_CHECKOUT=$1
THIS_CHECKOUT=$(pwd)

if ! [[ -d $BASE_CHECKOUT ]]; then
	echo "Specify a path to the base solidus checkout" >&2
	exit 1
fi

echo "Mounting ${BASE_CHECKOUT} as base"
echo "Mounting ${THIS_CHECKOUT} as overlay"
echo "Mounting at ${MOUNT_PATH}"

mkdir -p ${MOUNT_PATH} ${WORK_PATH}
mount -t overlay -o lowerdir=${BASE_CHECKOUT},upperdir=${THIS_CHECKOUT},workdir=${WORK_PATH} none ${MOUNT_PATH}
